<?php

namespace App\Imports;

use App\Models\Task;

use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\WithValidation;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\SkipsOnError;
use Maatwebsite\Excel\Concerns\SkipsErrors;
use Maatwebsite\Excel\Concerns\SkipsFailures;
use Maatwebsite\Excel\Concerns\SkipsOnFailure;
use Maatwebsite\Excel\Concerns\WithBatchInserts;
use Maatwebsite\Excel\Concerns\WithChunkReading;
//use Illuminate\Contracts\Queue\ShouldQueue;
use Maatwebsite\Excel\Validators\Failure;
use Throwable;

class TaskImport implements ToModel ,
                    WithHeadingRow,
                    SkipsOnError,
                    WithValidation,
                    SkipsOnFailure,
                    WithBatchInserts,
                    WithChunkReading

{
    use Importable,SkipsErrors,SkipsFailures;

    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function rules(): array
    {
            return [
                '*.module_code' => 'required',
                '*.module_name' => 'required',
                '*.module_term' => 'required',
            ];
    }
    public function model(array $row)
    {
        return new Task([
            'module_code'    => $row['module_code'],
            'module_name'    => $row['module_name'],
            'module_term'    => $row['module_term'],
        ]);
    }
    public function batchSize(): int
    {
        return 1000;
    }
    public function chunkSize(): int
    {
        return 1000;
    }
     public  function onError(Throwable $error){}
//    public  function onFailure(Failure ...$failure){
//    }

}
