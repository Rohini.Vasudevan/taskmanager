<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
Use App\Models\Task;
use App\Imports\TaskImport;
use Illuminate\Support\Facades\Mail;
use App\Mail\OrderShipped;
use Validator;

class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Task::orderBy('id', 'desc')->get();
    }

    public function store(Request $request)
    {
        if(!empty($request->file('file'))){
            $file = $request->file('file')->store('import');
            $extension = $request->file->getClientOriginalExtension();
            $import = new TaskImport;
            $data = array();
            if($extension == "csv"){
                $import->import($file);// dd($import->failures());
                if($import->failures()->isNotEmpty()){
                    foreach ($import->failures() as $errors){
                        array_push($data,$errors->attribute(). " is missing at row ". $errors->row());
                    }
                    //$user = 'rohinivasudevan26@gmail.com';
                    //Mail::to('$user')->send(new OrderShipped());
                    // Mail::send('mail', $user, function($message) use ($user) {
                    // $message->to($user);
                    // $message->subject('Welcome Mail');
                    // });
                   // dd('Mail Send Successfully');
                    $response = [
                        "status" => false,
                        "message" => $data,
                        "statusCode" => 400
                    ];
                }else{
                    $response =[
                        "status" => true,
                        "message" => "Your file is imported successfully.",
                        "statusCode" => 200
                    ];
                }
                }else{
                    $response =[
                        "status" => false,
                        "message" => "File type is invaild.",
                        "statusCode" => 400
                    ];
            }
        }else{
            $response =[
                "status" => false,
                "message" => "File is required.",
                "statusCode" => 200
            ];
        }
        //\Excel::import(new TaskImport,$request->file('file'));
        return response()->json($response);
        }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
